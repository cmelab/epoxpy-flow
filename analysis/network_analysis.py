import numpy as np
import gsd
import gsd.fl
import gsd.hoomd
import networkx as nx
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib
#plt.style.use('ggplot')
#%matplotlib inline
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)
matplotlib.rc('axes', labelsize=20)
matplotlib.rc('lines', linewidth=2)

def find_indices(lst, condition):
    return [i for i, elem in enumerate(lst) if condition(elem)]

def save_network_data(job,nSamples=20):
    '''
        Performs network analysis on the final frame and saves important info in job document.
    '''
    gsd_path=job.fn('data.gsd')
    #print(job)
    f = gsd.fl.GSDFile(gsd_path, 'rb')
    t = gsd.hoomd.HOOMDTrajectory(f)
    n_frames = len(t)
    print('total frames ',n_frames)
    last_frame = n_frames-1
    cluster_distributions = []
    fig = plt.figure(figsize=(12,9))
    time_conv = job.sp.dcd_write
    snapshot = t[int(last_frame)]
    bonds = snapshot.bonds.group
    G = nx.MultiGraph(ts='time_step:{}, kT:{}'.format(last_frame*time_conv,job.sp.kT))
    bond_types = [snapshot.bonds.types[i] for i in snapshot.bonds.typeid]
    #print(bond_types)
    ab_indices = find_indices(bond_types, lambda e: e == 'A-B')
    ab_bonds = [bonds[i] for i in ab_indices]
    for bond in ab_bonds:
        G.add_edge(bond[0],bond[1])
    sorted_cc = [len(c) for c in sorted(nx.connected_components(G), key=len, reverse=False)]
    num_clusters=len(sorted_cc)
    if len(sorted_cc) > 0:
        largest_network=sorted_cc[-1]
        if len(sorted_cc)==1:
            second_largest_network=sorted_cc[-1]
        else:
            second_largest_network=sorted_cc[-2]
        average_network_len=np.mean(sorted_cc)
    else:
        largest_networks.append(0)
        second_largest_networks.append(0)
    n, bins, patches = plt.hist(sorted_cc, 50, normed=1, facecolor='green', alpha=0.75)
    plt.xlabel('Cluster Length',fontsize=40)
    plt.ylabel('Probability',fontsize=40)
    #plt.plot(time_steps, average_network_lens,color='m',linestyle='-',marker='o',label='average cluster')
    plt.savefig(job.fn('clusters_hist.png'),transparent=True)
    job.document['largest_network']=largest_network
    job.document['second_largest_network']=second_largest_network
    job.document['average_network']=average_network_len
    job.document['network_size_std']=np.std(sorted_cc)
    if np.std(sorted_cc)==0.0:
        print('sorted_cc',sorted_cc,np.std(sorted_cc))
        job.document['network_size_sem']=0
    else:
        job.document['network_size_sem']=stats.sem(sorted_cc)
    job.document['num_clusters']=num_clusters
