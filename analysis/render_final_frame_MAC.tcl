set num [molinfo top get numframes]
set frame 0
display resetview
pbc wrap -center origin
pbc box -color black -center origin -width 6
rotate x by 15
rotate y by 15
display update
render TachyonInternal vmdscene.tga "/usr/local/bin/convert vmdscene.tga -fuzz 0% -trim -transparent white ./final_snapshot.png"
exit