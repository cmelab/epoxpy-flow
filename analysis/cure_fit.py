import numpy as np
import math
from scipy.optimize import curve_fit

def f_t(times,C,H,Ea,kT,a_start,a_inf,breakAt_a=None,model='FO'):
        #print('C,H',C,H)
        alphas = []
        minutes = []
        alpha=a_start
        #a_inf = 0.96
        for t in times:
            k = H*math.exp(-Ea/(kT))
            if model == 'SAFO':
                dadt = k*(a_inf-alpha)*(1+C*alpha)
            elif model == 'FO':
                dadt = k*(a_inf-alpha)
            elif model == 'SO':
                dadt = k*(a_inf-alpha)**2
            elif model == 'SASO':
                dadt = k*(1-alpha)*(a_inf-alpha)*(1+C*alpha)
            #print(dadt)
            alpha += dadt
            alphas.append(alpha)
            minutes.append(t/60.)
            #print(alpha,a_inf)
            
            if (breakAt_a is not None) and (alpha >= breakAt_a):
                t_minutes = t/60
                print('{} reached @ {} minutes according to the model'.format(alpha,t_minutes))
                return alphas,minutes,t_minutes
                #    print('done at',t)
                #    break
        return alphas

def fit_curing_profile_with_model(job,model):
    C=8.12 #Temperature independent acceleration constant. Aldridge, M., Wineman, A., Waas, A. & Kieffer, J.  (2014).
    num_func_groups_in_amines=4
    data = np.genfromtxt(job.fn('out.log'),names=True)
    bond_percents = data['bond_percentAB']
    time_steps = data['timestep']
    alpha_inf = job.sp.stop_after_percent
    
    truncated_cure_fractions = []
    truncated_time_steps = []

    # We cut off the cure profile after stop_after_percent because we want the cure profile to be realistic. 
    # After we stop bonding the cure profile is just flat and not realistic.
    last_index = next((i for i, v in enumerate(bond_percents) if v >=alpha_inf), -1)
    first_index = next((i for i, v in enumerate(bond_percents) if v >0), -1)
    if last_index<=0:#maybe the system did not cure till the desired cure percent. So just take the last cure of the profile
        last_index=len(bond_percents)
    if last_index > 0 and first_index >= 0 and (last_index-first_index)>1:
        truncated_time_steps.extend(time_steps[first_index:last_index]*job.sp.md_dt)#/0.01)
        truncated_cure_fractions.extend(bond_percents[first_index:last_index]/100.)
        Ea=job.sp.activation_energy#1.0
        a_inf = truncated_cure_fractions[-1]#0.96
        import warnings
        np.seterr(all='raise')
        plot_fit_fails=True
        label='Successfully fit the curing curve'
        success=False
        try:
            popt, pcov = curve_fit(lambda times, H: f_t(times, C, H,Ea,job.sp.kT,truncated_cure_fractions[0],a_inf,model=model),
                               truncated_time_steps,truncated_cure_fractions,
                               p0=[1],
                               bounds=([0],[np.infty]))
            success=True
        except FloatingPointError:
            label='FITTING FAILED DUE TO FLOATINGPOINTERROR'
        except RuntimeError:
            label='FITTING FAILED DUE TO RUNTIMEERROR'
        except TypeError:
            label='FITTING FAILED DUE TO TYPEERROR'
        except ValueError:
            label='FITTING FAILED DUE TO VALUEERROR'
        print(label,'for',model)
        if success:
            ydata = np.asarray(truncated_cure_fractions)
            fit_ydata = f_t(truncated_time_steps,C,*popt,Ea,job.sp.kT,truncated_cure_fractions[0],a_inf,model=model)
            residuals = ydata - fit_ydata
            ss_res = np.sum(residuals**2)
            ss_tot = np.sum((ydata-np.mean(ydata))**2)
            #print('ss_res',ss_res,'ss_tot',ss_tot)
            if ss_tot == 0:
                r_squared = 0
            else:
                r_squared = 1 - (ss_res / ss_tot)
            H = popt[0]
        else:
            r_squared=None
            H=None
    else:
        print('Did not try fitting curing curve because there are only 2 data points until {} percent curing'.format(alpha_inf))
        A=job.sp.num_a*job.sp.n_mul*num_func_groups_in_amines*job.sp.percent_bonds_per_step/100/job.sp.bond_period
        print('frequency factor for {} is {}(percent bonds:{},bond period:{})'.format(job,A,job.sp.percent_bonds_per_step,job.sp.bond_period))
        #. last_index:',last_index,'first_index',first_index)
        r_squared=None
        H=None
        success=False

    return success,r_squared,H
        
def saveCureFits(job):
    models = ['FO','SAFO','SO','SASO']
    for model in models:
        Rs = []
        NonZeroAs = []
        ns = []
        success=False
        if job.sp.profile == 'iso':
            success, r_squared,H = fit_curing_profile_with_model(job,model)
        else:
            print('Did not fit curing profiles because curing profile was',job.sp.profile)
        if success:
            job.document['{}_model_R2'.format(model)]=r_squared
            job.document['{}_model_H'.format(model)]=H
        else:
            job.document['{}_model_R2'.format(model)]=None
            job.document['{}_model_H'.format(model)]=None

