import numpy as np
import gsd
import gsd.fl
import gsd.hoomd
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib
#plt.style.use('ggplot')
#%matplotlib inline
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)
matplotlib.rc('axes', labelsize=20)
matplotlib.rc('lines', linewidth=2)

def find_indices(lst, condition):
    return [i for i, elem in enumerate(lst) if condition(elem)]

def save_gel_point(job,cluster_size_threshold=0.1,nSamples=20):
    '''
        cluster_size_threshold: set the threshold for the fractional difference in the largest and 
        second largest cluster sizes. The size difference at the end of the simulation is the point
        of comparison. So, if the size difference atleast 10 percent of the final size difference the previous 
        point is considered the gel point.
    '''
    gsd_path=job.fn('data.gsd')
    #print(job)
    f = gsd.fl.GSDFile(gsd_path, 'rb')
    t = gsd.hoomd.HOOMDTrajectory(f)
    n_frames = len(t)
    print('total frames ',n_frames)
    last_frame = n_frames-1
    time_steps=[]
    frame_ids = np.linspace(0,last_frame,nSamples,endpoint=True,dtype=int)
    cluster_distributions = []
    largest_networks = []
    second_largest_networks = []
    average_network_lens = []
    cluster_size_differences = []
    fig = plt.figure(figsize=(12,9))
    time_conv = job.sp.dcd_write
    print('frame ids',frame_ids)
    for frame_id in frame_ids:#range(0,n_frames,nSamplingInterval):
        #print((ts))
        ts=frame_id*time_conv
        time_steps.append(ts)
        snapshot = t[int(frame_id)]
        bonds = snapshot.bonds.group
        G = nx.MultiGraph(ts='time_step:{}, kT:{}'.format(last_frame*time_conv,job.sp.kT))
        bond_types = [snapshot.bonds.types[i] for i in snapshot.bonds.typeid]
        #print(bond_types)
        ab_indices = find_indices(bond_types, lambda e: e == 'A-B')
        ab_bonds = [bonds[i] for i in ab_indices]
        for bond in ab_bonds:
            G.add_edge(bond[0],bond[1])
        sorted_cc = [len(c) for c in sorted(nx.connected_components(G), key=len, reverse=False)]
        if len(sorted_cc) > 0:
            largest_networks.append(sorted_cc[-1])
            if len(sorted_cc)==1:
                second_largest_networks.append(sorted_cc[-1])
            else:
                second_largest_networks.append(sorted_cc[-2])
            #average_network_lens.append(np.mean(sorted_cc))
        else:
            largest_networks.append(0)
            second_largest_networks.append(0)
        cluster_size_differences.append(largest_networks[-1]-second_largest_networks[-1])
    #print(cluster_size_differences)
    plt.plot(time_steps, largest_networks,color='r',linestyle='-',marker='o',label='largest cluster')
    plt.plot(time_steps, second_largest_networks,color='b',linestyle='-',marker='o',label='second largest cluster')
    plt.legend(fontsize=20)
    plt.xlabel('Time steps',fontsize=40)
    plt.ylabel('Cluster length',fontsize=40)
    #plt.plot(time_steps, average_network_lens,color='m',linestyle='-',marker='o',label='average cluster')
    plt.savefig(job.fn('two_largest_clusters.png'),transparent=True)

    diffs = [next_one-current for current, next_one in zip(cluster_size_differences, cluster_size_differences[1:])]
    if np.sum(diffs) == 0:
        gel_index=len(time_steps)-1
        print('detected zero difference. So setting gel frame to the last one',gel_index,len(time_steps))
    else:
        #print('diffs',diffs)
        final_diff = cluster_size_differences[-1]
        if final_diff == 0:
            final_diff = np.mean(cluster_size_differences[-3:])
            if final_diff == 0:
                final_diff=1
        #print(diffs)
        gel_index = next(i for i,diff in enumerate(diffs) if diff/final_diff >cluster_size_threshold)
    #print('gel_index',gel_index)
    gel_point = time_steps[gel_index]
    #print('gel_point',gel_point)
    job.document['gel_point']=gel_point
    data = np.genfromtxt(job.fn('out.log'),names=True)
    cure_percents = data['bond_percentAB']
    time_steps_in_log = data['timestep']
    times_after_gelation = find_indices(time_steps_in_log,lambda e: e>=gel_point)
    if len(times_after_gelation)>0:
        time_id_closest_to_gelation = times_after_gelation[0]
    else:
        print('ERROR: unexpected condition in cure percent at gel point calculation')
    cure_at_gelation = cure_percents[time_id_closest_to_gelation]
    job.document['curing_at_gel_point'] = cure_at_gelation
    plt.axvline(x=gel_point,linestyle='--')
    plt.savefig(job.fn('two_largest_clusters.png'),transparent=True)

