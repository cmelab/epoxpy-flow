import gsd
import gsd.fl
import gsd.hoomd
from freud import parallel, box, density
import numpy as np

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import cme_utils.manip.pbc as pbc
import cme_utils.manip.convert_rigid as convert
parallel.setNumThreads(4)# I did a performance test and found 4 to be the optimal number of threads for 50K particles.

matplotlib.rc('xtick', labelsize=50)
matplotlib.rc('ytick', labelsize=50)
matplotlib.rc('axes', labelsize=20)
matplotlib.rc('lines', linewidth=2)

def calcCOM(listOfPositions, listOfMasses):
    massWeighted = np.array([0.0, 0.0, 0.0])
    totalMass = np.sum(listOfMasses)
    for atomID, position in enumerate(listOfPositions):
        for axis in range(3):
            massWeighted[axis] += position[axis] * listOfMasses[atomID]
    return massWeighted / float(totalMass)

def batch_gen(data, batch_size):
    for i in range(0, len(data), batch_size):
            yield data[i:i+batch_size]

def getRDFFromGSD(gsdFileName,
            type1,
            type2,
            rmax=None,
            dr=0.1,
            nFrames=10,
            type1_COM=False,
            type1_N=1,
            type2_COM=False,
            type2_N=1):
    '''
    Creates an averaged RDF over the last N frames given by nFrames for type1 and type2.
    If type1 or type2 is a bead chain rather than single particles, type<#>_COM flag
    allows user to use the center of mass of the bead chain of length type<#>_N.

    Arguments:
    type1: typeid of the first particle type (integer)
    type2: typeid of the second particle type (integer)
    rmax: Sets the maximum distance to calculate RDF for. If set to None, we automatically choose
    L/2 as rmax. Default: None
    dr: Sets the interval of distance at which RDF is evaluated. Control the resolution of the RDF. Default: 0.1
    nFrames: Sets the number of frames over which the RDF is averaged. The algorithms picks up the
    last nFrames based on this value.
    type1_COM,type2_COM: Specifies if the particle type is a bead chain and hence need to use the center
    of mass than the position of a single bead. This assumes that the bead chain just consists of
    beads of a single type and is of length type1_N or type2_N respectively.

    Returns: An rdf object defined in the freud library(http://glotzerlab.engin.umich.edu/freud/density.html#freud.density.RDF)
    '''
    f = gsd.fl.GSDFile(gsdFileName, 'rb')
    t = gsd.hoomd.HOOMDTrajectory(f)
    if rmax is None and len(t) > 0:
        snapshot = t[0]
        sim_box = snapshot.configuration.box
        rmax = sim_box[0]/2
    rdf = density.RDF(rmax=rmax, dr=dr)
    rdf.resetRDF()
    n_frames = len(t)
    end_frame = n_frames-1
    start_frame=end_frame-nFrames
    print('RDF will be calulated from frame {} to {}. Available frames are from {} to {}.'.format(start_frame,end_frame,0,n_frames-1))
    box_dim=(sim_box[0], sim_box[1], sim_box[2])
    box_dim = np.array(box_dim)
    print('box_dim',box_dim)
    #iterate over the frames to calculate average
    for i in range(start_frame, end_frame+1):
        snapshot = t[i]
        sim_box = snapshot.configuration.box
        fbox = box.Box(Lx=sim_box[0], Ly=sim_box[1], Lz=sim_box[2])
        l_pos = snapshot.particles.position
        l_masses = snapshot.particles.mass
        l_images = snapshot.particles.image
        type1_pos = l_pos[np.where(snapshot.particles.typeid == type1)]
        type2_pos = l_pos[np.where(snapshot.particles.typeid == type2)]
        if type1_COM is True:
            type1_masses = l_masses[np.where(snapshot.particles.typeid == type1)]
            COMS = []
            for positions,masses in zip(batch_gen(type1_pos, type1_N),
                                               batch_gen(type1_masses,type1_N)):
                unwrapped_pos = convert.pbc_traslate(positions,box_dim)
                COM = [calcCOM(unwrapped_pos,masses)]
                wrappedCOM,thisImage = pbc.shift_pbc(COM,box_dim)
                COMS.append(wrappedCOM[0]) #COM
            type1_pos = np.array(COMS,dtype=np.float32)
        if type2_COM is True:
            type2_masses = l_masses[np.where(snapshot.particles.typeid == type2)]
            COMS = []
            for positions,masses in zip(batch_gen(type2_pos, type2_N),
                                               batch_gen(type2_masses,type2_N)):
                unwrapped_pos = convert.pbc_traslate(positions,box_dim)
                COM = [calcCOM(unwrapped_pos,masses)]
                wrappedCOM,thisImage = pbc.shift_pbc(COM,box_dim)
                COMS.append(wrappedCOM[0]) #COM
                #print('wrapped p',positions,'\nunwrapped ps',unwrapped_pos)
                #print('unwrapped COM',COM,'wrapped COM',wrappedCOM)
            type2_pos = np.array(COMS,dtype=np.float32)
        rdf.accumulate(fbox, type1_pos, type2_pos)
    return rdf

def saveRDFs(job, nFrames=10, dr=0.05, maxY=2.5):
    #print(job)
    axis_label_size=40
    if 'neat' in job.sp.sim_name:
        rdf =getRDFFromGSD(job.fn('data.gsd'),type1=0,type2=0,nFrames=nFrames,dr=dr)
        r=rdf.getR()
        gr =rdf.getRDF()
        fig = plt.figure(figsize=(15,10))
        plt.xlabel(r"$r$",fontsize=axis_label_size)
        plt.ylabel(r"$g\left(r\right)$",fontsize=axis_label_size)
        plt.ylim(0,maxY)
        plt.legend(fontsize=20)
        plt.plot(r,gr,label='DDS-DDS',color='r')
        fig.savefig(job.fn('rdf.png'))
    else:
        rdf1 = getRDFFromGSD(job.fn('data.gsd'),type1=0,type2=0,nFrames=nFrames,dr=dr)
        r_DDS_DDS=rdf1.getR()
        gr_DDS_DDS =rdf1.getRDF()
        fig = plt.figure(figsize=(15,10))
        plt.xlabel(r"$r$",fontsize=axis_label_size)
        plt.ylabel(r"$g\left(r\right)$",fontsize=axis_label_size)
        plt.ylim(0,maxY)
        plt.legend(fontsize=20)
        plt.plot(r_DDS_DDS,gr_DDS_DDS,label='DDS-DDS',color='r')
        fig.savefig(job.fn('rdf_DDS_DDS.png'))

        rdf2 = getRDFFromGSD(job.fn('data.gsd'),type1=1,type2=1,nFrames=nFrames,dr=dr)
        r_DEGBA_DEGBA=rdf2.getR()
        gr_DEGBA_DEGBA =rdf2.getRDF()
        fig = plt.figure(figsize=(15,10))
        plt.xlabel(r"$r$",fontsize=axis_label_size)
        plt.ylabel(r"$g\left(r\right)$",fontsize=axis_label_size)
        plt.ylim(0,maxY)
        plt.legend(fontsize=20)
        plt.plot(r_DEGBA_DEGBA,gr_DEGBA_DEGBA,label='DEGBA-DEGBA',color='b')
        fig.savefig(job.fn('rdf_DEGBA_DEGBA.png'))

        rdf3 = getRDFFromGSD(job.fn('data.gsd'),type1=2,type2=2,
                           type1_COM=True,type1_N=10,
                           type2_COM=True,type2_N=10,
                           nFrames=nFrames,dr=dr)
        r_PES_PES=rdf3.getR()
        gr_PES_PES =rdf3.getRDF()
        fig = plt.figure(figsize=(15,10))
        plt.xlabel(r"$r$",fontsize=axis_label_size)
        plt.ylabel(r"$g\left(r\right)$",fontsize=axis_label_size)
        plt.ylim(0,maxY)
        plt.legend(fontsize=20)
        plt.plot(r_PES_PES,gr_PES_PES,label='PES-PES',color='g')
        fig.savefig(job.fn('rdf_PES_PES.png'))

        rdf4 = getRDFFromGSD(job.fn('data.gsd'),type1=0,type2=1,nFrames=nFrames,dr=dr)
        r_DDS_DEGBA=rdf4.getR()
        gr_DDS_DEGBA =rdf4.getRDF()
        fig = plt.figure(figsize=(15,10))
        plt.xlabel(r"$r$",fontsize=axis_label_size)
        plt.ylabel(r"$g\left(r\right)$",fontsize=axis_label_size)
        plt.ylim(0,maxY)
        plt.legend(fontsize=20)
        plt.plot(r_DDS_DEGBA,gr_DDS_DEGBA,label='DDS-DEGBA',color='purple')
        fig.savefig(job.fn('rdf_DDS_DEGBA.png'))

        rdf5 = getRDFFromGSD(job.fn('data.gsd'),type1=0,type2=2,type2_COM=True,type2_N=10,nFrames=nFrames,dr=dr)
        r_DDS_PES=rdf5.getR()
        gr_DDS_PES =rdf5.getRDF()
        fig = plt.figure(figsize=(15,10))
        plt.xlabel(r"$r$",fontsize=axis_label_size)
        plt.ylabel(r"$g\left(r\right)$",fontsize=axis_label_size)
        plt.ylim(0,maxY)
        plt.legend(fontsize=20)
        plt.plot(r_DDS_PES,gr_DDS_PES,label='DDS-PES',color='r')
        fig.savefig(job.fn('rdf_DDS_PES.png'))

        rdf6 = getRDFFromGSD(job.fn('data.gsd'),type1=1,type2=2,type2_COM=True,type2_N=10,nFrames=nFrames,dr=dr)
        r_DEGBA_PES=rdf6.getR()
        gr_DEGBA_PES =rdf6.getRDF()
        fig = plt.figure(figsize=(15,10))
        plt.xlabel(r"$r$",fontsize=axis_label_size)
        plt.ylabel(r"$g\left(r\right)$",fontsize=axis_label_size)
        plt.ylim(0,maxY)
        plt.legend(fontsize=20)
        plt.plot(r_DEGBA_PES,gr_DEGBA_PES,label='DEGBA-PES',color='b')
        fig.savefig(job.fn('rdf_DEGBA_PES.png'))

        fig = plt.figure(figsize=(15,10))
        plt.plot(r_DDS_DEGBA,gr_DDS_DEGBA,label='DDS-DEGBA',color='purple')
        plt.plot(r_DDS_PES,gr_DDS_PES,label='DDS-PES',color='r')
        plt.plot(r_DEGBA_PES,gr_DEGBA_PES,label='DEGBA-PES',color='b')
        plt.plot(r_DDS_DDS,gr_DDS_DDS,label='DDS-DDS',linestyle='--',color='r')
        plt.plot(r_DEGBA_DEGBA,gr_DEGBA_DEGBA,label='DEGBA-DEGBA',linestyle='--',color='b')
        plt.plot(r_PES_PES,gr_PES_PES,label='PES-PES',linestyle='--',color='g')

        plt.xlabel(r"$r$",fontsize=40)
        plt.ylabel(r"$g\left(r\right)$",fontsize=40)
        plt.ylim(0,maxY)
        plt.legend(fontsize=20)
        fig.savefig(job.fn('rdf.png'))

        fig = plt.figure(figsize=(15,12))
        plt.plot(r_DDS_DEGBA,gr_DDS_DEGBA,label='DDS-DEGBA',color='purple')
        plt.plot(r_DDS_PES,gr_DDS_PES,label='DDS-PES',color='r')
        plt.plot(r_DEGBA_PES,gr_DEGBA_PES,label='DEGBA-PES',color='b')

        plt.xlabel(r"$r$",fontsize=40)
        plt.ylabel(r"$g\left(r\right)$",fontsize=40)
        plt.ylim(0,maxY)
        plt.legend(fontsize=20)
        fig.savefig(job.fn('rdf_cross.png'))
