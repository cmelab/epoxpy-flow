#!/usr/bin/env python3
from signac_dashboard import Dashboard
from signac_dashboard.modules import ImageViewer


class MyDashboard(Dashboard):

    def job_title(self, job):
        title='job_name'
        if 'curing_job' in job.sp:
            job_type='quench job'
            title='{}_qT_{}'.format(job_type, job.sp['quench_T'])
        else:
            job_type='curing_job'
            title = '{}_kT_{}_gamma_{}'.format(job_type, job.sp['kT'],job.sp.gamma)
        return title

#MyDashboard().run()
if __name__ == '__main__':
    dashboard = MyDashboard(modules=[ImageViewer()])
    dashboard.run(host='localhost', port=8888)
