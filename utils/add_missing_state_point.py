import signac
import gsd
import gsd.fl
import gsd.hoomd
import numpy as np

project = signac.get_project('../')

for job in project:
#   if 'gamma' not in job.sp:
#            job.sp.gamma = 4.5
#   if 'Lx' not in job.document:
#       if job.isfile('data.gsd'):
#           f = gsd.fl.GSDFile(job.fn('data.gsd'), 'rb')
#           t = gsd.hoomd.HOOMDTrajectory(f)
#           snapshot = t[0]
#           job.document['Lx'] = float(snapshot.configuration.box[0])
#           job.document['Ly'] = float(snapshot.configuration.box[1])
#           job.document['Lz'] = float(snapshot.configuration.box[2])
#       else:
#           print(job,'data.gsd not found')
#   if 'pot' not in job.sp:
#       job.sp.pot = 'DPD'
#   if 'shrink_time' not in job.sp:
#       job.sp.shrink_time = 1e6
#   if 'mix_dt' not in job.sp:
#       job.sp.mix_dt = job.sp.dt
#   if 'md_dt' not in job.sp:
#       job.sp.md_dt = job.sp.dt
    if job.sp.pot == 'LangH':
        print(job,job.sp.kT,len(np.arange(0.1,job.sp.kT+0.05,0.05)))
        job.sp.n_quench_Ts=len(np.arange(0.1,job.sp.kT+0.05,0.05))

