#!/usr/bin/env python
"""Initialize the project's data space.

Iterates over all defined state points and initializes
the associated job workspace directories."""
import signac
import numpy as np
import epoxpy.temperature_profile_builder as tpb
import itertools
import epoxpy.common as cmn


project = signac.init_project('EpoxpyProject')
quench_time = 1e7 
cooling_method='anneal'#'anneal'#'quench'
search_criteria = {'bond':True,"kT":3.0}
print('search criteria:',search_criteria)
cure_jobs = project.find_jobs(search_criteria)
P=10.0
tau_factor = 100
tauP_factor = 1000

for cure_job in cure_jobs:
   if cooling_method=='quench':
       fine_quench_time = 3e6
       coarse_quench_T_start = 0.5
       coarse_quench_T_end = 0.2
       coarse_quench_T_interval = 0.1
       coarse_quench_Ts = np.arange(coarse_quench_T_start,coarse_quench_T_end,-coarse_quench_T_interval)
       print('coarse_quench_Ts',coarse_quench_Ts)
       for coarse_quench_T in coarse_quench_Ts:
           fine_quench_interval = 0.01
           fine_quench_T_end = coarse_quench_T-coarse_quench_T_interval
           fine_quench_Ts = np.arange(coarse_quench_T-fine_quench_interval,fine_quench_T_end,-fine_quench_interval)
           print('fine_quench_Ts',fine_quench_Ts)
           search_criteria = {'bond':False,
                             "stop_after_percent":stop_after_percent,
                             "quench_time":quench_time,
                             "quench_T":round(coarse_quench_T,3)}
           print('search criteria:',search_criteria)
           coarse_quench_jobs = project.find_jobs(search_criteria)
           if len(coarse_quench_jobs)==1:
               coarse_quench_job = coarse_quench_jobs.next()
               print('found coarse quench jobs for \nqT:',coarse_quench_T,
                       '\nstop_after_percent:',stop_after_percent,
                       '\nquench_time:',quench_time)
               print('coarse quench job:',coarse_quench_job)
               for fine_quench_T in fine_quench_Ts:
                   fine_quench_T = round(fine_quench_T,3)
                   sp = coarse_quench_job.statepoint()
                   qp = tpb.LinearTemperatureProfileBuilder(initial_temperature=fine_quench_T)
                   #print('Quenching',cure_job,'to',quenchT)
                   qp.add_state_point(fine_quench_time,fine_quench_T)
                   sp['cooling_method']=cooling_method
                   sp['quench_method']='fine_quench'
                   sp['curing_job']=coarse_quench_job.get_id()
                   sp['curing_job_sp']=coarse_quench_job.statepoint()#cure_job.get_id()
                   sp['quench_temp_prof']=qp.get_raw()
                   sp['quench_T']=fine_quench_T
                   sp['quench_time']=fine_quench_time
                   #sp['ext_init_struct_path']=init_file
                   sp['bond']=False
                   sp['mix_time']=0
                   sp['integrator']=cmn.Integrators.NPT.name
                   sp['use_curing_job_P']=False#use_curing_job_P
                   sp['P']=P
                   sp['tau']=sp['md_dt']*tau_factor
                   sp['tauP']=sp['md_dt']*tauP_factor
                   project.open_job(sp).init()
           else:
               print('Did not find coarse quench jobs for \nqT:',coarse_quench_T,
                       '\nstop_after_percent:',stop_after_percent,
                       '\nquench_time:',quench_time)
               raise ValueError('Found {} coarse quench jobs. Expected only one. Please check filter'.format(len(coarse_quench_jobs)))
   elif cooling_method == 'anneal':
       print('Using anneal method for cooling')
       fine_quench_time = 6e6
       coarse_quench_T_start = 2.5
       coarse_quench_T_end = 0.5
       coarse_quench_T_interval = 2.0
       coarse_quench_Ts = [2.5]#np.arange(coarse_quench_T_start,coarse_quench_T_end,-coarse_quench_T_interval)
       print('coarse_quench_Ts',coarse_quench_Ts)
       for coarse_quench_T in coarse_quench_Ts:
           fine_quench_interval = 0.1
           fine_quench_T_end = coarse_quench_T-coarse_quench_T_interval
           print('cure job stop_after_percent',cure_job.sp.stop_after_percent)
           search_criteria = {'bond':False,
                             "curing_job_sp":cure_job.statepoint(),
                             "quench_time":quench_time,
                             "quench_T":round(coarse_quench_T,3)}
           print('search criteria:',search_criteria)
           coarse_quench_jobs = project.find_jobs(search_criteria)
           if len(coarse_quench_jobs)==1:
               coarse_quench_job = coarse_quench_jobs.next()
               print('found coarse quench jobs for \nqT:',coarse_quench_T,
                       '\nstop_after_percent:',coarse_quench_job.sp.stop_after_percent,
                       '\nquench_time:',quench_time)
               print('coarse quench job:',coarse_quench_job)
               qp = tpb.StepwiseQuenchTemperatureProfileBuilder(initial_temperature=coarse_quench_T,
                                                                final_temperature=fine_quench_T_end,
                                                                quench_time=fine_quench_time,
                                                                first_step_buffer_time=0,
                                                                quench_interval=fine_quench_interval)

               sp = coarse_quench_job.statepoint()
               sp['curing_job']=coarse_quench_job.get_id()
               sp['curing_job_sp']=coarse_quench_job.statepoint()
               sp['quench_temp_prof']=qp.get_raw()
               sp['quench_T']=round(fine_quench_T_end,2)
               sp['cooling_method']=cooling_method
               sp['quench_method']='fine_quench'
               sp['quench_time']=fine_quench_time
               sp['integrator']=cmn.Integrators.NPT.name
               project.open_job(sp).init()
           else:
               print('Did not find coarse quench jobs for \nqT:',coarse_quench_T,
                       '\nstop_after_percent:',cure_job.sp.stop_after_percent,
                       '\nquench_time:',quench_time)
               raise ValueError('Found {} coarse quench jobs. Expected only one. Please check filter'.format(len(coarse_quench_jobs)))

project.write_statepoints()
