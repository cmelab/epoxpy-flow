from flow import FlowProject
from flow import staticlabel
import environment
from flow import get_environment
import signac

class EpoxpyProject(FlowProject):

    def __init__(self, *args, **kwargs):
        super(EpoxpyProject, self).__init__(*args, **kwargs)
        env = get_environment()
        print(env);
        self.add_operation(
            name='prep',
            cmd=lambda job: "python operations.py initialize {}".format(job),
            post=[EpoxpyProject.prep])
        self.add_operation(
            name='process',
            cmd=lambda job: "python -u operations.py mix_and_cure {}".format(job),
            pre=[EpoxpyProject.prep],
            post=[EpoxpyProject.mixed,EpoxpyProject.processed])
        self.add_operation(
            name='post_process',
            cmd=lambda job: "python -u operations.py post_process {}".format(job),
            pre=[EpoxpyProject.processed],#,EpoxpyProject.quenched],
            post=[EpoxpyProject.post_processed])

    @staticlabel()
    def prep(job):
        return job.isfile('temperature_profile.png')

    @staticlabel()
    def mixed(job):
        return job.isfile('mixed.hoomdxml')

    @staticlabel()
    def processed(job):
        return job.isfile('final.hoomdxml')

    @staticlabel()
    def post_processed(job):
        def sys_info_present_in_doc(job):
            system_info_present = 'volume' in job.document and\
                                      'temperature' in job.document and\
                                      'pressure' in job.document and\
                                      'Lx' in job.document and\
                                      'Ly' in job.document and\
                                      'Lz' in job.document
            if (job.sp.bond) or ('curing_job_sp' in job.sp):
                system_info_present = system_info_present and ('cure_percent' in job.document)
            return system_info_present

        if 'curing_job' in job.sp:
            sys_info = sys_info_present_in_doc(job)
            all_done = sys_info
            all_done = all_done and 'D_A' in job.document
            return all_done
        else:
            sys_info_done =  sys_info_present_in_doc(job)
            all_done = sys_info_done
            if job.sp.bond:
                gel_point_calculated = (job.isfile('two_largest_clusters.png'))
                cure_fitting_calculated = ('FO_model_R2' in job.document)
                bonded_post_process_done = (gel_point_calculated and cure_fitting_calculated)
            else:
                bonded_post_process_done = True
            all_done = bonded_post_process_done and all_done
            all_done = all_done and 'average_network' in job.document and 'network_size_std' in job.document
            all_done = all_done and job.isfile('asq.png')  # final snapshot diffract
            all_done = all_done and job.isfile('final_snapshot.png')
            all_done = all_done and job.isfile('qm.txt')  # first peak evolution wrt time

            ##if job.sp.n_mul <= 1e3:
            ##    rdfs_calculated = job.isfile('rdf.png')
            ##else:
            #rdfs_calculated = True#tricking the system!
            #diffusivity_calculated = ('D_A' in job.document)
            #species_concentration_calculated = (job.isfile('amine_concentration.png'))
            #non_bonded_post_processing_done = (rdfs_calculated and \
            #                                   diffusivity_calculated and \
            #                                   species_concentration_calculated and \
            #                                   sq_calculated)
            #all_done = (bonded_post_process_done and non_bonded_post_processing_done)
            return  all_done

if __name__ == '__main__':
    EpoxpyProject().main()
