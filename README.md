# About

This repository provides a template for [epoxpy-flow](git@bitbucket.org:cmelab/epoxy_sim.git) projects based on the simple data management framework [signac](https://signac.readthedocs.io/).

# Dependencies
* signac-flow
* freud 
* cme_utils
* matplotlib


# Installation

epoxpy-flow can be easily installed through [conda](https://conda.io/docs/install/quick.html#miniconda-quick-install-requirements). It is tested for python 3.5.

### Installing using conda

```
conda create --name epoxpy python=3.5
source activate epoxpy
conda install -c glotzer signac-flow
conda install -c glotzer freud
conda install -c glotzer gsd
```

#### Install cme_utils using pip

```
mkdir projects
cd projects
git clone git@bitbucket.org:cmelab/cme_utils.git
cd cme_utils
pip install -e .
```

# Quickstart

Start by initializing the project and a few state points with the `init.py` module.
```
python init.py 
```
The number 42 is the random seed used for initialization, feel free to replace it with a different number or a text string, which will be converted into a numeric random seed.

To submit the jobs on a cluster the 'submit' command is used 
```
python EpoxpyProject.py submit -n 1
```
num specifies the number of jobs to submit at once

We can checkout the project's status with the `status.py` module.
```
python EpoxpyProject.py status --detailed --parameters p
```
We use the ``--detailed`` flag to show the labels explicitly for each job.
The ``--parameters`` (``-p``) pargument specifies state point parameters that should be shown in the status overview.
In this case we specify to show the `p` variable, which stands for pressure.
